# foma-stuff

An experiment in implementing Ŋarâþ Crîþ v9 morphology using [PyFoma](https://github.com/mhulden/pyfoma).

Currently incomplete because everything about Ŋarâþ Crîþ hates you.

[Post on Ŋarâþ Crîþ site describing the project.](https://ncv9.flirora.xyz/diary/0F96-fomacation.html)

## Getting started

```sh
python -m venv .
bin/pip install pyfoma
bin/python ncv9.py
```

## Architecture

### Special symbols

#### `:` and `-`

The only explicit marking of *cþenros* states is the use of a colon (`:`) between syllables. This means that:

* You can find the start of a syllable using `#|':'`
* You can find the end of a simple syllable using `':'`

Hyphens are unflavored in this project; this is intentional because state-dependent hyphen behavior is now regarded as a bad idea. It is not yet decided whether `-:` and `:-` are different, and if not, which one should be used.

#### Other morphophonological symbols

`Phi` is the phi consonant; it gets replaced by `f` or `ł` in the `ResolvePhi` stage depending on the properties of the preceding stem.

`F`, `Fn`, `Ft`, and `Fþ` specify stem fusion. The ones without the leading `<` are used before hyphens, and the ones with the leading `<` (`<F`, `<Fn`, `<Ft`, and `<Fþ`) are used after hyphens. In fact, the latter are translated into the former in `ReorderFusionCommands` by swapping them with preceding hyphens.

#### Categories

* Case: `[Nom]`, `[Acc]`, `[Dat]`, `[Gen]`, `[Loc]`, `[Inst]`, `[Abess]`, `[Sembl]`
* Number: `[Di]`, `[Du]`, `[Pl]`, `[St]`, `[Gc]`

#### Component separation

Components are customarily separated with `/`, though the order of the components varies by paradigm. The helper expression `$chunk` gets a string without any `/`. There are also extraction helpers such as `$stem03` (gets component 0 from the first 3 and follows it with a hyphen).

### Sets

The following useful sets are available in `defs`:

| Name | Details |
|------|---------|
| `$plain` | Plain consonants |
| `$epf` | Effective plosives and fricatives (i.e. those that can form onsets with ⟦l⟧ or ⟦r⟧) |
| `$lenitable` | Consonants that can be lenited |
| `$Iota` | Initials |
| `$Mu` | Medials (optional ⟦j⟧) |
| `$Onset` | Onsets (initials plus an optional ⟦j⟧) |
| `$Nu` | Vowels |
| `$Kappa` | Nonterminal codas |
| `$Omega` | Terminal codas |
| `$MSyll` | Medial syllables |

### Transformations

The following transformations are defined in `defs`:

| Name | ŊC symbol | Definition |
|------|-----------|------------|
| `$pi` | π | ~aoe/oei |
| `$gamma` | γ | ~aoei/eeie |
| `$plus` | + | ~o/a |
| `$eta` | η | aeio/âêîô |
| `$uneta` | n/a | inverse of `$eta` |

All of these take any vowel; vowels not specified to change remain unchanged.
