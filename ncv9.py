from pyfoma import FST, Paradigm

grammar = {}
grammar["S"] = [("", "Noun"), ("", "Verb")]
grammar["Noun"] = [
    ("ga:ši:d/ga:šjo:d/gel:ši:d/a/a", "N_I"),
    ("el:t/il:t/el:d/es/a", "N_I"),
    ("ŋa:r/ŋôr:þ/ŋa:l/âþ/a", "N_I"),
    ("to:vr/te:vr/te:v/a/a", "N_I"),
]
grammar["Verb"] = []
grammar["N_I"] = [("'[N.I]'", "N_Case")]
grammar["N_Case"] = [("'[Nom]'", "N_Num")]
grammar["N_Num"] = [("'[Di]'", "#"), ("'[Du]'", "#"), ("'[Pl]'", "#"), ("'[St]'", "#"), ("'[Gc]'", "#")]

lexicon = FST.rlg(grammar, "S")
lexicon = lexicon.epsilon_remove().determinize().minimize()

defs = {'lexicon': lexicon}

# Helper expression for getting the parts we want
defs['chunk'] = FST.re("[^/]*", defs)
defs['stem03'] = FST.re("$chunk ('/' $chunk '/' $chunk '/'):'-'", defs)

# Vowel transformations
defs['Nu'] = FST.re("[aeiouâêîô]", defs)
defs['pi'] = FST.re("a:o|â:ô|o:e|ô:ê|e:i|ê:î|[iîu]", defs)
defs['gamma'] = FST.re("[aei]:e|[âêî]:ê|o:i|ô:î|u", defs)
defs['eta'] = FST.re("a:â|e:ê|i:î|o:ô|u", defs)
defs['uneta'] = FST.re("$^reverse($eta)", defs)
defs['plus'] = FST.re("o:a|ô:â|[aeiuâêî]", defs)

# Phonotactic categories
defs['plain'] = FST.re("[cnŋvsþšrlłmfgptčdðhħ]", defs)
defs['epf'] = FST.re("[cvsþšfgptdðhħ]", defs)
defs['lenitable'] = FST.re("[ptdčcgmfvð]", defs)
defs['Iota'] = FST.re("''|$plain|$epf[rl]|$lenitable'·'|($lenitable&$epf)'·'[rl]|cf|cþ|cs|cš|gv|gð|tf|dv", defs)
defs['Mu'] = FST.re("j?", defs)
defs['Onset'] = FST.re("$Iota$Mu", defs)
defs['Kappa'] = FST.re("[srnþltcfł]|rþ|cþ", defs)
defs['Omega'] = FST.re("$Kappa|st|lt|ns|ls|nþ|łt|m", defs)
defs['MSyll'] = FST.re("$Onset$Nu$Kappa':'", defs)

# Determination of phi-consonants
defs['łtc'] = FST.re("[pfvmgdðh]", defs)
defs['łton'] = FST.re("($łtc|($epf&$łtc)[rl]|($lenitable&$łtc)'·'|($lenitable&$epf&$łtc)'·'[rl]|cf|gv|gð|tf|dv)j?", defs)
defs['phił'] = FST.re("(.*[^lł]':'$łton)|(($MSyll*&(.*[pfvm]))$Onset)", defs)

# First-declension nouns
defs['NITheme'] = FST.re("[aeo]|([ae][sn]:'')|((â:a|ê:e)þ:'')", defs)
defs['NIEnd'] = FST.re("$^input($NITheme)", defs)
defs['NIThemeNomPl'] = FST.re("([aeo]@$pi) | (([ae]@$gamma)s) | (([âê]@$pi)þ) | (([ae]@$pi)(n:''))", defs)
defs['NIThemeNomSt'] = FST.re("(([aeo]@$gamma)'':l) | ('':'<Fn'([ae]@$gamma)s) | ('':'<Fn'([âê]@$gamma)þ) | ('':'<Fn'([ae]@$gamma)(n:l))", defs)
defs["NINomDi"] = FST.re("$stem03 $NIEnd ('/' $chunk '[N.I]' '[Nom]' '[Di]'):''", defs)
defs["NINomDu"] = FST.re("$stem03 $NITheme '':c ('/' $chunk '[N.I]' '[Nom]' '[Du]'):''", defs)
defs["NINomPl"] = FST.re("$stem03 $NIThemeNomPl ('/' $chunk '[N.I]' '[Nom]' '[Pl]'):''", defs)
defs["NINomSt"] = FST.re("$stem03 $NIThemeNomSt ('/' $chunk '[N.I]' '[Nom]' '[St]'):''", defs)
defs["NINomGc"] = FST.re("$stem03 ($NITheme@$plus) '':'Phi' ('/' $chunk '[N.I]' '[Nom]' '[Gc]'):''", defs)
defs["NI"] = FST.re("$NINomDi | $NINomDu | $NINomPl | $NINomSt | $NINomGc", defs)

# Most of the time, the first morpheme will be the stem, but in case it’s not, we can mark its beginning with the <Stem> symbol.
defs["ResolvePhi"] = FST.re("$^rewrite('Phi':'ł' / (# | '<Stem>') $phił '-' .* _) @ $^rewrite('Phi':'f') @ $^rewrite('<Stem>':'')", defs)
defs["ReorderFusionCommands"] = FST.re("$^rewrite('-':'' ('<Fn':'Fn' | '<Ft':'Ft' | '<Fþ':'Fþ' | '<F' : 'F') '':'-')", defs)
defs["PrepareMorph"] = FST.re("$ResolvePhi @ $ReorderFusionCommands", defs)

ncv9 = FST.re("$lexicon @ $NI @ $PrepareMorph", defs)

print(Paradigm(ncv9, '.*'))
ncv9.render()
